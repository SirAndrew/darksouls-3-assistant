package com.example.darksouls3assistant;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class browser extends AppCompatActivity implements View.OnClickListener {

    DatabaseHelper databaseHelper;
    SQLiteDatabase db;
    Cursor cursor;
    String Request;
    static TableLayout table;

    public static int player_strength = 0;
    public static int player_dexterity = 0;
    public static int player_intelligence = 0;
    public static int player_faith = 0;

    public final int INTEGER_ID = 1;
    public final int FLOAT_ID = 2;
    public final int STRING_ID = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browser_activity);
        databaseHelper = new DatabaseHelper(getApplicationContext());
        // создаем базу данных

        PlayerStatsRequest(); //получение характеристик игрока

        /* НАДО ОСТАВЛЯТЬ ПРОБЕЛ В КОНЦЕ ЗАПРОСА ДЛЯ СОРТИРОВКИ*/
        switch (MainActivity.requestId){
            case MainActivity.WeaponRequestId:
                WeaponRequest();
                break;
            case MainActivity.GameObjectRequestId:
                GameObjectRequest();
                break;
            case MainActivity.PlayerStatsUpdateId:
                PlayerStatsUpdate();
                break;
            case MainActivity.WeaponFromNpcRequestId:
                WeaponFromNpcRequest();
                break;
            case MainActivity.EnemyOnLocationRequestId:
                EnemyOnLocationRequest();
                break;
            case MainActivity.BossSoulsRequestId:
                BossSoulsRequest();
                break;
            case MainActivity.ArmorRequestId:
                ArmorRequest();
                break;
            case MainActivity.ItemRequestId:
                ItemRequest();
                break;
            case MainActivity.RingRequestId:
                RingRequest();
                break;
            case MainActivity.BossSoulWeaponRequestId:
                BossSoulWeaponRequest();
                break;
            case MainActivity.EnemyRequestId:
                EnemyRequest();
                break;
            case MainActivity.WeaponOnLocationRequestId:
                WeaponOnLocationRequest();
                break;
            case MainActivity.WeaponFromEnemyRequestId:
                WeaponFromEnemyRequest();
                break;
            case MainActivity.LocationRequestId:
                LocationRequest();
                break;
            case MainActivity.NpcRequestId:
                NpcRequest();
                break;
            case MainActivity.InsertRequestId:
                InsertRecord();
                NpcRequest();
                break;
            case MainActivity.DeleteRequestId:
                DeleteRecord();
                NpcRequest();
                break;
        }
    }

    public void WeaponRequest(){
        Request = "select game_object.name as weapon_name, damage, magic_damage, fire_damage, lightning_damage, dark_damage, critical, str_required as strength_required, \n" +
                " dex_required as dexterity_required, int_required as intelligence_required, \n" +
                "faith_required, str_bonus as strength_bonus, dex_bonus as dexterity_bonus,int_bonus as intelligence_bonus, faith_bonus, bleed, poison, frost, weight \n" +
                " from weapon INNER JOIN game_object on game_object.id = weapon.game_object_id INNER join weapon_type on weapon.weapon_type_id = weapon_type.id ";
        RequestConstructor(Request,true,false);
    }

    public void GameObjectRequest(){
        Request = "select name, price from game_object ";
        RequestConstructor(Request,true,true);
    }

    public void WeaponFromNpcRequest(){
        Request = "SELECT game_object_to_npc.id, game_object.name as weapon_name, npc.name as npc_name,game_object.price  FROM game_object_to_npc INNER JOIN game_object on game_object_to_npc.game_object_id=game_object.id \n" +
                " INNER JOIN npc ON game_object_to_npc.npc_id =  npc.id where game_object.type_id=0 ";
        RequestConstructor(Request,true,false);
    }

    public void EnemyOnLocationRequest(){
        Request = "select enemy.name as enemy_name, location.name as location_name from enemy_to_location inner join enemy on enemy.id = enemy_to_location.id inner join location on location.id = enemy_to_location.id ";
        RequestConstructor(Request,true, true);
    }

    public void BossSoulsRequest(){
        Request = "select enemy.name as boss_name, enemy.hp as hp, enemy.souls as souls, boss_soul.name as soul_name from boss inner join enemy on enemy.id = boss.enemy_id \n" +
                " inner join boss_soul on boss_soul.id = boss.boss_soul_id ";
        RequestConstructor(Request,true, false);
    }

    public void ArmorRequest(){
        Request = "select game_object.name as armor_name, armor_type.type as armor_type, phys_res as physical_resistance, mag_res as magical_resistance, fire_res as fire_resistance, \n" +
                "light_res as lightning_resistance, dark_res as dark_resistance, poise, bleed_res as bleed_resistance, poison_res as poison_resistance, freeze_res as freeze_resistance, \n" +
                "curse_res as curse_resistance, weight from armor inner join game_object on armor.game_object_id = game_object.id inner join armor_type on armor.armor_type = armor_type.id ";
        RequestConstructor(Request,true, false);
    }

    public void ItemRequest() {
        Request = "select game_object.name as name, item.usage from item inner join game_object on item.game_object_id = game_object.id ";
        RequestConstructor(Request,true,false);
    }

    public void RingRequest(){
        Request = "select game_object.name as name, ring.effect from ring inner join game_object on ring.game_object_id = game_object.id ";
        RequestConstructor(Request,true,false);
    }

    public void BossSoulWeaponRequest(){
        Request = "select game_object.name as weapon_name, boss_soul.name as soul_name from boss_soul_weapon inner join boss_soul on boss_soul_weapon.boss_soul_id = boss_soul.id inner join weapon on boss_soul_weapon.weapon_id = weapon.id \n" +
                "inner join game_object on weapon.id = game_object.id ";
        RequestConstructor(Request,true,true);
    }

    public void EnemyRequest(){
        Request = "select name as enemy_name, hp, souls from enemy ";
        RequestConstructor(Request,true,false);
    }

    public void NpcRequest(){
        Request = "select npc.name as npc_name, location.name as location_name from npc inner join location on location.id = npc.location_id ";
        RequestConstructor(Request,true,true);
    }

    public void LocationRequest(){
        Request = "select name from location ";
        RequestConstructor(Request,true,true);
    }

    public void WeaponFromEnemyRequest(){
        Request = "select game_object.name as game_object_name, enemy.name as enemy_name from game_object_to_enemy inner join game_object on game_object.id = game_object_to_enemy.game_object_id\n" +
                " inner join enemy on enemy.id = game_object_to_enemy.enemy_id ";
        RequestConstructor(Request,true,true);
    }

    public void WeaponOnLocationRequest(){
        Request = "select game_object.name as game_object_name, location.name as location_name from game_object_to_location inner join game_object on game_object.id = game_object_to_location.game_object_id \n" +
                "inner join location on location.id = game_object_to_location.location_id ";
        RequestConstructor(Request,true,true);
    }

    public void PlayerStatsRequest(){
        databaseHelper.create_db();
        // открываем подключение
        db = databaseHelper.open();
        Request = "SELECT * FROM player";
        cursor = db.rawQuery(Request, null);
        cursor.moveToFirst();

        player_strength = cursor.getInt(cursor.getColumnIndex("Strength"));
        player_dexterity = cursor.getInt(cursor.getColumnIndex("Dexterity"));
        player_intelligence = cursor.getInt(cursor.getColumnIndex("Intelligence"));
        player_faith = cursor.getInt(cursor.getColumnIndex("Faith"));
        db.close();
    }

    public void PlayerStatsUpdate(){
        databaseHelper.create_db();
        // открываем подключение
        db = databaseHelper.open();

        //настройка таблицы
        table = findViewById(R.id.table);
        table.removeAllViews();
        table.setStretchAllColumns(true);
        table.setShrinkAllColumns(true);

        //лейблы
        TableRow label = new TableRow(this);
        TableLayout.LayoutParams tableRowParams=
                new TableLayout.LayoutParams
                        (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        tableRowParams.setMargins(1, 1, 1, 1);
        label.setLayoutParams(tableRowParams);
        label.setBackgroundColor(Color.WHITE);

        TextView strLabel = new TextView(this);
        strLabel.setText("Strength");
        label.addView(strLabel);

        TextView dexLabel = new TextView(this);
        dexLabel.setText("Dexterity");
        label.addView(dexLabel);

        TextView intLabel = new TextView(this);
        intLabel.setText("Intelligence");
        label.addView(intLabel);

        TextView faithLabel = new TextView(this);
        faithLabel.setText("Faith");
        label.addView(faithLabel);

        TextView empty = new TextView(this);
        label.addView(empty);

        table.addView(label);

        //старые статы
        TableRow oldStats = new TableRow(this);
        oldStats.setLayoutParams(tableRowParams);
        oldStats.setBackgroundColor(Color.WHITE);

        TextView oldstr = new TextView(this);
        oldstr.setText(Integer.toString(player_strength));
        oldStats.addView(oldstr);

        TextView olddex = new TextView(this);
        olddex.setText(Integer.toString(player_dexterity));
        oldStats.addView(olddex);

        TextView oldint = new TextView(this);
        oldint.setText(Integer.toString(player_intelligence));
        oldStats.addView(oldint);

        TextView oldfaith = new TextView(this);
        oldfaith.setText(Integer.toString(player_faith));
        oldStats.addView(oldfaith);

        TextView empty1 = new TextView(this);
        oldStats.addView(empty1);

        table.addView(oldStats);

        //новые статы
        TableRow newStats = new TableRow(this);
        newStats.setLayoutParams(tableRowParams);
        newStats.setBackgroundColor(Color.WHITE);

        final EditText newstr = new EditText(this);
        newstr.setInputType(InputType.TYPE_CLASS_NUMBER );
        newStats.addView(newstr);

        final EditText newdex = new EditText(this);
        newdex.setInputType(InputType.TYPE_CLASS_NUMBER );
        newStats.addView(newdex);

        final EditText newint = new EditText(this);
        newint.setInputType(InputType.TYPE_CLASS_NUMBER );
        newStats.addView(newint);

        final EditText newfaith = new EditText(this);
        newfaith.setInputType(InputType.TYPE_CLASS_NUMBER );
        newStats.addView(newfaith);

        Button add = new Button(this);
        add.setText("Добавить");
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.create_db();
                // открываем подключение
                db = databaseHelper.open();
                int str = Integer.parseInt(newstr.getText().toString());
                int dex = Integer.parseInt(newdex.getText().toString());
                int inte = Integer.parseInt(newint.getText().toString());
                int faith = Integer.parseInt(newfaith.getText().toString());

                /*Request = "update player set Strength =" + str+ " , Dexterity = " + dex+ ", Intelligence = " + inte+ ", Faith =" + faith;
                db.execSQL(Request);*/

                String sql = "update player set Strength = ?, Dexterity = ?, Intelligence = ?, Faith = ?";
                SQLiteStatement statement = db.compileStatement(sql);

                statement.bindLong(1,str);
                statement.bindLong(2,dex);
                statement.bindLong(3,inte);
                statement.bindLong(4,faith);
                statement.executeUpdateDelete();

                db.close();
                Toast toast = Toast.makeText(getApplicationContext(), "Данные обновлены", Toast.LENGTH_LONG);
                toast.show();
                PlayerStatsRequest();
                PlayerStatsUpdate();

            }
        });
        newStats.addView(add);
        table.addView(newStats);
        cursor.close();
        db.close();
    }

    public void InsertRecord(){
        databaseHelper.create_db();
        db = databaseHelper.open();
        String sql = "INSERT INTO npc (name, location_id) VALUES ('Неразрывный Лоскутик', 6)";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.executeInsert();
    }

    public void DeleteRecord(){
        databaseHelper.create_db();
        db = databaseHelper.open();
        String sql = "delete from npc where npc.name = 'Неразрывный Лоскутик'";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.executeUpdateDelete();
    }

    public void RequestConstructor(String req, final boolean sorting, final boolean shrink){
        databaseHelper.create_db();
        // открываем подключение
        db = databaseHelper.open();

        final String inc_req = req;
        int counter = 1;
        table = findViewById(R.id.table);

        if(shrink){
            table.setStretchAllColumns(true);
            table.setShrinkAllColumns(true);
        }

        TableRow label = new TableRow(this);
        TableLayout.LayoutParams tableRowParams=
                new TableLayout.LayoutParams
                        (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        tableRowParams.setMargins(1, 1, 1, 1);
        label.setLayoutParams(tableRowParams);
        label.setBackgroundColor(Color.WHITE);
        //получаем данные из бд в виде курсора
        cursor = db.rawQuery(req, null);
        cursor.moveToFirst();
        int count_columns = cursor.getColumnCount();
        final String [] column_names = cursor.getColumnNames();

        //создаю строку с названиями столбцов
        for (int i = 0; i < count_columns; i++){
            if(sorting){ //если нужно сортировать, создаю кнопки
                Button col_label = new Button(this);
                col_label.setText(column_names[i] + " ");
                final int current_column = i;
                col_label.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        table.removeAllViews();
                        final String newReq;

                        if(inc_req.equals(Request + "order by " + column_names[current_column])){
                            newReq = inc_req.substring(0, inc_req.indexOf("order by")) + "order by " + column_names[current_column] + " desc";
                        }
                        else {
                            if (inc_req.indexOf("order by") == -1){
                                newReq = inc_req + "order by " + column_names[current_column];
                            }
                            else {
                                newReq = inc_req.substring(0, inc_req.indexOf("order by")) + "order by " + column_names[current_column];
                            }

                        }
                        RequestConstructor(newReq, sorting, shrink);
                    }
                });
                label.addView(col_label);
            }
            else { //если не нужно сортировать, создаю текстовые поля
                TextView col_label = new TextView(this);
                col_label.setText(column_names[i] + " ");
                label.addView(col_label);
            }
        }
        table.addView(label);
        //обработка каждой строки
        while (!cursor.isAfterLast()) {
            final TableRow tableRow = new TableRow(this);
            TableLayout.LayoutParams WhiteTheme=
                    new TableLayout.LayoutParams
                            (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
            WhiteTheme.setMargins(1, 1, 1, 1);
            tableRow.setBackgroundColor(Color.WHITE);
            tableRow.setLayoutParams(WhiteTheme);

            //заполнение столбцов
            for (int i = 0; i < count_columns; i++){
                int type = cursor.getType(i);
                switch (type){
                    case STRING_ID:
                        String Str = cursor.getString(cursor.getColumnIndex(column_names[i]));
                        TextView StrView = new TextView(this);
                        StrView.setText(Str);
                        tableRow.addView(StrView);
                        break;
                    case INTEGER_ID:
                        int integer = cursor.getInt(cursor.getColumnIndex(column_names[i]));
                        TextView integerView = new TextView(this);
                        integerView.setText(Integer.toString(integer)!= null? Integer.toString(integer) : "-");
                        if(column_names[i].equals("strength_required")){
                            if(integer>player_strength){
                                integerView.setBackgroundColor(Color.RED);
                            }
                        }
                        if(column_names[i].equals("dexterity_required")){
                            if(integer>player_dexterity){
                                integerView.setBackgroundColor(Color.RED);
                            }
                        }
                        if(column_names[i].equals("intelligence_required")){
                            if(integer>player_intelligence){
                                integerView.setBackgroundColor(Color.RED);
                            }
                        }
                        if(column_names[i].equals("faith_required")){
                            if(integer>player_faith){
                                integerView.setBackgroundColor(Color.RED);
                            }
                        }
                        tableRow.addView(integerView);
                        break;
                    case FLOAT_ID:
                        float f = cursor.getFloat(cursor.getColumnIndex(column_names[i]));
                        TextView fView = new TextView(this);
                        fView.setText(Float.toString(f));
                        tableRow.addView(fView);
                        break;
                    default:
                        TextView def = new TextView(this);
                        def.setText("-");
                        tableRow.addView(def);
                }
            }
            //подсветка выделенной строки по нажатию
            tableRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < table.getChildCount();i++){
                        table.getChildAt(i).setBackgroundColor(Color.WHITE);
                    }
                    tableRow.setBackgroundColor(Color.GREEN);
                }
            });
            table.addView(tableRow, counter);
            counter++;
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
    }
    @Override
    public void onClick(View view) {

    }
}
