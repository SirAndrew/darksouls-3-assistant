package com.example.darksouls3assistant;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {



    public static int requestId;

    public static final int PlayerStatsUpdateId = 0;
    public static final int WeaponRequestId=1;
    public static final int GameObjectRequestId =2;
    public static final int BossSoulsRequestId =3;
    public static final int ArmorRequestId = 4;
    public static final int RingRequestId = 5;
    public static final int ItemRequestId = 6;
    public static final int BossSoulWeaponRequestId = 7;
    public static final int EnemyRequestId = 8;
    public static final int WeaponOnLocationRequestId = 9;
    public static final int WeaponFromNpcRequestId = 10;
    public static final int EnemyOnLocationRequestId = 11;
    public static final int WeaponFromEnemyRequestId = 12;
    public static final int LocationRequestId = 13;
    public static final int NpcRequestId = 14;
    public static final int InsertRequestId = 15;
    public static final int DeleteRequestId = 16;


    Button WeaponButton;
    Button GameObjectButton;
    Button PlayerStatsButton;
    Button WeaponFromNpcButton;
    Button EnemyOnLocationButton;
    Button BossSoulButton;
    Button ArmorButton;
    Button RingButton;
    Button ItemButton;
    Button BossSoulWeaponButton;
    Button EnemyButton;
    Button WeaponOnLocationButton;
    Button WeaponFromEnemyButton;
    Button LocationButton;
    Button NpcButton;
    Button InsertButton;
    Button DeleteButton;

    DatabaseHelper databaseHelper;
    SQLiteDatabase db;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WeaponButton = findViewById(R.id.weapon_btn);
        WeaponButton.setOnClickListener(this);
        WeaponFromNpcButton = findViewById(R.id.weapon_from_npc_btn);
        WeaponFromNpcButton.setOnClickListener(this);
        EnemyOnLocationButton = findViewById(R.id.enemy_on_location_btn);
        EnemyOnLocationButton.setOnClickListener(this);
        BossSoulButton = findViewById(R.id.boss_souls_btn);
        BossSoulButton.setOnClickListener(this);
        PlayerStatsButton = findViewById(R.id.player_stats_update);
        PlayerStatsButton.setOnClickListener(this);
        GameObjectButton = findViewById(R.id.game_object_btn);
        GameObjectButton.setOnClickListener(this);
        ArmorButton = findViewById(R.id.armor_btn);
        ArmorButton.setOnClickListener(this);
        RingButton = findViewById(R.id.ring_btn);
        RingButton.setOnClickListener(this);
        ItemButton = findViewById(R.id.item_btn);
        ItemButton.setOnClickListener(this);
        BossSoulWeaponButton = findViewById(R.id.boss_soul_weapon_btn);
        BossSoulWeaponButton.setOnClickListener(this);
        EnemyButton = findViewById(R.id.enemy_btn);
        EnemyButton.setOnClickListener(this);
        WeaponOnLocationButton = findViewById(R.id.weapon_on_location_btn);
        WeaponOnLocationButton.setOnClickListener(this);
        WeaponFromEnemyButton = findViewById(R.id.weapon_from_enemy_btn);
        WeaponFromEnemyButton.setOnClickListener(this);
        LocationButton = findViewById(R.id.location_btn);
        LocationButton.setOnClickListener(this);
        NpcButton = findViewById(R.id.npc_btn);
        NpcButton.setOnClickListener(this);
        InsertButton = findViewById(R.id.insert_btn);
        InsertButton.setOnClickListener(this);
        DeleteButton = findViewById(R.id.delete_btn);
        DeleteButton.setOnClickListener(this);


        databaseHelper = new DatabaseHelper(getApplicationContext());
        databaseHelper.create_db();
        // открываем подключение
        db = databaseHelper.open();
        cursor = db.rawQuery("select * from weapon", null);
        int count = cursor.getCount();
        WeaponButton.setText("Оружие (" + Integer.toString(count)+")");

        cursor = db.rawQuery("select * from armor", null);
        count = cursor.getCount();
        ArmorButton.setText("Броня (" + Integer.toString(count)+")");

        cursor = db.rawQuery("select * from ring", null);
        count = cursor.getCount();
        RingButton.setText("Кольца (" + Integer.toString(count)+")");

        cursor = db.rawQuery("select * from item", null);
        count = cursor.getCount();
        ItemButton.setText("Предметы (" + Integer.toString(count)+")");

        cursor = db.rawQuery("select * from game_object", null);
        count = cursor.getCount();
        GameObjectButton.setText("Игровые объекты (" + Integer.toString(count)+")");

    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, browser.class);
        switch (view.getId()){
            case R.id.weapon_btn:
                requestId = WeaponRequestId;
                break;
            case R.id.game_object_btn:
                requestId = GameObjectRequestId;
                break;
            case R.id.player_stats_update:
                requestId = PlayerStatsUpdateId;
                break;
            case R.id.weapon_from_npc_btn:
                requestId = WeaponFromNpcRequestId;
                break;
            case R.id.enemy_on_location_btn:
                requestId = EnemyOnLocationRequestId;
                break;
            case R.id.boss_souls_btn:
                requestId = BossSoulsRequestId;
                break;
            case R.id.armor_btn:
                requestId = ArmorRequestId;
                break;
            case R.id.ring_btn:
                requestId = RingRequestId;
                break;
            case R.id.item_btn:
                requestId = ItemRequestId;
                break;
            case R.id.boss_soul_weapon_btn:
                requestId = BossSoulWeaponRequestId;
                break;
            case R.id.enemy_btn:
                requestId = EnemyRequestId;
                break;
            case R.id.weapon_on_location_btn:
                requestId = WeaponOnLocationRequestId;
                break;
            case R.id.weapon_from_enemy_btn:
                requestId = WeaponFromEnemyRequestId;
                break;
            case R.id.location_btn:
                requestId = LocationRequestId;
                break;
            case R.id.npc_btn:
                requestId = NpcRequestId;
                break;
            case R.id.insert_btn:
                requestId = InsertRequestId;
                break;
            case R.id.delete_btn:
                requestId = DeleteRequestId;
                break;
        }
        startActivity(intent);
    }
}
